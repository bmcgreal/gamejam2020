extends Area2D

# this script controls the Player
# this tutuorial was used to develop the grid based movement:
# https://kidscancode.org/godot_recipes/2d/grid_movement/

# node variables:
onready var HUD = get_parent().get_node("HUD_holder/HUD")
onready var level_clear_popup = get_parent().get_node("LevelClearPopup")
onready var ray = $RayCast2D
onready var tween = $Tween
onready var anim_player = $AnimationPlayer
onready var sprite = $Sprite
export var speed = 7
var tileWidth = 32
var facing_left = true
var can_teleport = true

var inputs = {"player_up_w": Vector2.UP,
				"player_left_a": Vector2.LEFT,
				"player_down_s": Vector2.DOWN,
				"player_right_d": Vector2.RIGHT,
				"player_up_up": Vector2.UP,
				"player_left_left": Vector2.LEFT,
				"player_down_down": Vector2.DOWN,
				"player_right_right": Vector2.RIGHT}
# the inputs can be expaned later
var ups: int
var downs: int
var lefts: int
var rights:int
var coins: int
var needed_coins: int

# Called when the node enters the scene tree for the first time.
func _ready():
	coins = 0
	# not sure what these two lines do, probably can be deleted
	#position = position.snapped(Vector2.ONE * tileWidth)
	#position += Vector2.ONE * tileWidth/2\

func _unhandled_input(event):
	if tween.is_active():
		return  # ignore inputs while tween is running
	for dir in inputs.keys():
		if event.is_action_pressed(dir):
			move(dir)

func move(dir):
	# use ray to check for collisions
	ray.cast_to = inputs[dir] * tileWidth
	ray.force_raycast_update()
	if !ray.is_colliding():
		if inputs[dir] == Vector2.UP and get_ups() > 0:
			sub_up()
			move_tween(dir)
			play_anim("walk_up")
		elif inputs[dir] == Vector2.DOWN and get_downs() > 0:
			sub_down()
			move_tween(dir)
			play_anim("walk")
		elif inputs[dir] == Vector2.LEFT and get_lefts() > 0:
			sub_left()
			move_tween(dir)
			facing_left= false
			play_anim("walk")
		elif inputs[dir] == Vector2.RIGHT and get_rights() > 0:
			sub_right()
			move_tween(dir)
			facing_left = true
			play_anim("walk")

func play_anim(anim_name):
	if anim_player.is_playing() and anim_player.current_animation == anim_name:
		return
	anim_player.play(anim_name)
	
func move_tween(dir):
	tween.interpolate_property(self, "position",
		position, position + inputs[dir] * tileWidth,
		1.0/speed, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.start()
	can_teleport = true

func reposition_tween(new_pos):
	set_collision_layer_bit(0,false)
	set_collision_layer_bit(1,true)
	set_collision_mask_bit(0,false)
	set_collision_mask_bit(1,true)
	tween.interpolate_property(self, "position", position, new_pos, 1.0/speed, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.start()

func set_moves(u, d, l, r):
	ups = u
	downs = d
	lefts = l
	rights = r
	HUD.update_up(ups)
	HUD.update_down(downs)
	HUD.update_left(lefts)
	HUD.update_right(rights)

func add_up():
	ups = ups + 1
	HUD.update_up(ups)

func sub_up():
	ups = ups - 1
	HUD.update_up(ups)

func get_ups():
	return ups

func add_down():
	downs = downs + 1
	HUD.update_down(downs)

func sub_down():
	downs = downs - 1
	HUD.update_down(downs)

func get_downs():
	return downs

func add_left():
	lefts = lefts + 1
	HUD.update_left(lefts)

func sub_left():
	lefts = lefts - 1
	HUD.update_left(lefts)

func get_lefts():
	return lefts

func add_right():
	rights = rights + 1
	HUD.update_right(rights)

func sub_right():
	rights = rights - 1
	HUD.update_right(rights)

func get_rights():
	return rights

func add_coin():
	coins = coins + 1
	#$CoinSound.play()
	HUD.update_coin(coins, needed_coins)

func get_coins():
	return coins

func set_needed_coins(n):
	needed_coins = n
	HUD.update_coin(coins, needed_coins)

func get_needed_coins():
	return needed_coins

func get_can_teleport():
	return can_teleport

func set_can_teleport(can_tele):
	can_teleport = can_tele

# this variable is temporary and should be removed when real win logic is added
var indicate_win_once = false

func _process(delta):
	if get_coins() >= get_needed_coins() and !indicate_win_once:
		indicate_win_once = true
		level_clear_popup.popup()
	sprite.set_flip_h(!facing_left)

func _on_Tween_tween_completed(object, key):
	set_collision_layer_bit(0,true)
	set_collision_layer_bit(1,false)
	set_collision_mask_bit(0,true)
	set_collision_mask_bit(1,false)
	play_anim("idle")
