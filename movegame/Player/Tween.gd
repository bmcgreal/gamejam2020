extends Tween

signal completed_tween

func _on_Tween_tween_completed(_object, _key):
	emit_signal("completed_tween")
