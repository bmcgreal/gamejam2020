extends Node2D

# general purpose variables
var mainViewportWidth
var mainViewportHeight
var gridWidth = 32
var gridHeight = 32
var tile_map_origin
var init_up = 2
var init_down = 0
var init_left = 7
var init_right = 4
var init_coins = 0
var init_goal = 2
var margin = 20
var next_level_path = "res://Levels/level_3_3.tscn"

# scene variables
onready var player = get_node("Player")
onready var tile_map = get_node("TileMap")
onready var level_clear_popup = get_node("LevelClearPopup")

# Called when the node enters the scene tree for the first time.
func _ready():
	setup_player(5,7)
	for child in get_node("TileMap").get_children():
		child.snap_to_grid()

	level_clear_popup.set_next_level_path(next_level_path)

func setup_player(spawn_row, spawn_col):
	player.set_moves(init_up,init_down,init_left,init_right)  # u d l r
	player.set_needed_coins(init_goal)
	tile_map_origin = Vector2(tile_map.position.x, tile_map.position.y)
	player.position.x = tile_map_origin[0] + ( spawn_col + 0.5 ) * gridWidth
	player.position.y = tile_map_origin[1] + ( spawn_row + 0.5 ) * gridWidth

func _unhandled_input(event):
	if event.is_action_pressed("reset_map"):
		get_tree().reload_current_scene()
	if event.is_action_pressed("return_to_menu"):
		get_tree().change_scene("res://MainMenu/LevelSelect.tscn")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
