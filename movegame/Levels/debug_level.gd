extends Node2D

# general purpose variables
var mainViewportWidth
var mainViewportHeight
var gridWidth = 32
var gridHeight = 32
var tile_map_origin

# set per-level initial state
var init_up = 99
var init_down = 99
var init_left = 99
var init_right = 99
var init_coins = 0
var init_goal = 99

# dont touch
var margin = 20

# scene variables
onready var player = get_node("Player")
onready var tile_map = get_node("TileMap")

# Called when the node enters the scene tree for the first time.
func _ready():
	# set  up resolution and framing:
	# vertical:

	"""
	var grid_origin = Vector2( tile_map.position.x, tile_map.position.y )
	var tile_rect = tile_map.get_used_rect()
	print(tile_rect)
	var move_up = tile_rect.position[1] * gridHeight + grid_origin[1] - margin
	var new_height = get_viewport().size.y - move_up + margin
	self.position.y -= move_up
	# horizontal:
	var half_length = ceil( tile_rect.size[0] / 2 ) * gridWidth + margin
	var new_half_width = max( half_length, 192 )  # compare to HUD keepout
	var move_left = (get_viewport().size.x / 2) - new_half_width
	self.position.x -= move_left
	OS.set_window_size(Vector2(new_half_width * 2, new_height))
	# change screen stretch stuff:
	get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_VIEWPORT, 
			SceneTree.STRETCH_ASPECT_EXPAND, Vector2(new_half_width * 2, new_height))

	# set size multiplier (probably only 1 or 2)
	var multiplier = 1.5
	OS.set_window_size( multiplier * Vector2(new_half_width * 2, new_height) )
	OS.set_window_position(Vector2(new_half_width, new_height*0.5))
	"""
	
	set_player_input_map()
	
	# set player location in grid (tilemap coordinates)
	# Example:
	# setup_player(10,13)
	setup_player(5,9)
	
	# obtain nodes using get_node(TileMap/NodeName)
	# call snap_to_grid() for each node
	# Example:
	#var coin_a = get_node("TileMap/Coin")
	#coin_a.snap_to_grid()
	var coin_1 = get_node("TileMap/Coin")
	coin_1.snap_to_grid()
	var coin_2 = get_node("TileMap/Coin2")
	coin_2.snap_to_grid()
	
	var right_arrow = get_node("TileMap/RightArrow")
	right_arrow.snap_to_grid()
	var left_arrow = get_node("TileMap/LeftArrow")
	left_arrow.snap_to_grid()
	var up_arrow = get_node("TileMap/UpArrow")
	up_arrow.snap_to_grid()
	var down_arrow = get_node("TileMap/DownArrow")
	down_arrow.snap_to_grid()
	
	var right_slider = get_node("TileMap/RightSlider")
	right_slider.snap_to_grid()
	var left_slider = get_node("TileMap/LeftSlider")
	left_slider.snap_to_grid()
	var up_slider = get_node("TileMap/UpSlider")
	up_slider.snap_to_grid()
	var down_slider = get_node("TileMap/DownSlider")
	down_slider.snap_to_grid()
	
	var teleporter_1 = get_node("TileMap/Teleporter")
	teleporter_1.snap_to_grid()
	var teleporter_2 = get_node("TileMap/Teleporter2")
	teleporter_2.snap_to_grid()
	teleporter_1.set_other_position(teleporter_2.get_this_position())
	teleporter_2.set_other_position(teleporter_1.get_this_position())

func setup_player(spawn_row, spawn_col):
	player.set_moves(init_up,init_down,init_left,init_right)  # u d l r
	player.set_needed_coins(init_goal)
	tile_map_origin = Vector2(tile_map.position.x, tile_map.position.y)
	player.position.x = tile_map_origin[0] + ( spawn_col + 0.5 ) * gridWidth
	player.position.y = tile_map_origin[1] + ( spawn_row + 0.5 ) * gridWidth

func _unhandled_input(event):
	if event.is_action_pressed("reset_map"):
		OS.set_window_size(Vector2(1024,600))
		get_tree().reload_current_scene()

func set_player_input_map():
	# WASD
	var wInput = InputEventKey.new()
	wInput.scancode = KEY_W
	InputMap.add_action("player_up_w")
	InputMap.action_add_event("player_up_w", wInput)
	var aInput = InputEventKey.new()
	aInput.scancode = KEY_A
	InputMap.add_action("player_left_a")
	InputMap.action_add_event("player_left_a", aInput)
	var sInput = InputEventKey.new()
	sInput.scancode = KEY_S
	InputMap.add_action("player_down_s")
	InputMap.action_add_event("player_down_s", sInput)
	var dInput = InputEventKey.new()
	dInput.scancode = KEY_D
	InputMap.add_action("player_right_d")
	InputMap.action_add_event("player_right_d", dInput)
	# arrow keys
	var upInput = InputEventKey.new()
	upInput.scancode = KEY_UP
	InputMap.add_action("player_up_up")
	InputMap.action_add_event("player_up_up", upInput)
	var leftInput = InputEventKey.new()
	leftInput.scancode = KEY_LEFT
	InputMap.add_action("player_left_left")
	InputMap.action_add_event("player_left_left", leftInput)
	var downInput = InputEventKey.new()
	downInput.scancode = KEY_DOWN
	InputMap.add_action("player_down_down")
	InputMap.action_add_event("player_down_down", downInput)
	var rightInput = InputEventKey.new()
	rightInput.scancode = KEY_RIGHT
	InputMap.add_action("player_right_right")
	InputMap.action_add_event("player_right_right", rightInput)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
