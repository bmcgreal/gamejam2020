extends Area2D


onready var player_tween = get_node("../../Player/Tween")
var grid_origin
var grid_width = 32

# Called when the node enters the scene tree for the first time.
func _ready():
	player_tween.connect("completed_tween", self, "tween_complete")

func snap_to_grid():
	grid_origin = Vector2( get_parent().position.x, get_parent().position.y )
	var x_diff = fmod(( self.position.x - grid_width / 2 - grid_origin[0] ) , grid_width )
	var y_diff = fmod(( self.position.y - grid_width / 2 - grid_origin[1] ) , grid_width )
	self.position.x -= x_diff
	self.position.y -= y_diff

func tween_complete():
	var overlaps = get_overlapping_areas()
	if overlaps.size() != 0:
		var player = overlaps[0]
		$SliderSound.play()
		player.move_tween("player_right_d")
