extends Node2D

# general purpose variables
var mainViewportWidth
var mainViewportHeight
var gridWidth = 32
var gridHeight = 32

# scene variables
var itemList = []
onready var player = get_node("Player")

# Called when the node enters the scene tree for the first time.
func _ready():
	mainViewportWidth = get_viewport().size.x
	mainViewportHeight = get_viewport().size.y
	set_player_input_map()
	setup_player()

func setup_player():
	player.set_moves(99,99,99,99)
	player.set_needed_coins(2)
	print(player.get_ups())

func set_player_input_map():
	# WASD
	var wInput = InputEventKey.new()
	wInput.scancode = KEY_W
	InputMap.add_action("player_up_w")
	InputMap.action_add_event("player_up_w", wInput)
	var aInput = InputEventKey.new()
	aInput.scancode = KEY_A
	InputMap.add_action("player_left_a")
	InputMap.action_add_event("player_left_a", aInput)
	var sInput = InputEventKey.new()
	sInput.scancode = KEY_S
	InputMap.add_action("player_down_s")
	InputMap.action_add_event("player_down_s", sInput)
	var dInput = InputEventKey.new()
	dInput.scancode = KEY_D
	InputMap.add_action("player_right_d")
	InputMap.action_add_event("player_right_d", dInput)
	# arrow keys
	var upInput = InputEventKey.new()
	upInput.scancode = KEY_UP
	InputMap.add_action("player_up_up")
	InputMap.action_add_event("player_up_up", upInput)
	var leftInput = InputEventKey.new()
	leftInput.scancode = KEY_LEFT
	InputMap.add_action("player_left_left")
	InputMap.action_add_event("player_left_left", leftInput)
	var downInput = InputEventKey.new()
	downInput.scancode = KEY_DOWN
	InputMap.add_action("player_down_down")
	InputMap.action_add_event("player_down_down", downInput)
	var rightInput = InputEventKey.new()
	rightInput.scancode = KEY_RIGHT
	InputMap.add_action("player_right_right")
	InputMap.action_add_event("player_right_right", rightInput)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
