extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var grid_origin
var grid_width = 32

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func snap_to_grid():
	grid_origin = Vector2( get_parent().position.x, get_parent().position.y )
	var x_diff = fmod(( self.position.x - grid_width / 2 - grid_origin[0] ) , grid_width )
	var y_diff = fmod(( self.position.y - grid_width / 2 - grid_origin[1] ) , grid_width )
	self.position.x -= x_diff
	self.position.y -= y_diff

func _on_UpArrow_area_entered(area):
	area.add_up()
	$ArrowSound.play()

func _on_ArrowSound_finished():
	self.queue_free()
