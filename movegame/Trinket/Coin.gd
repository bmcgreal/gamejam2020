extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var grid_origin
var grid_width = 32

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func snap_to_grid():
	grid_origin = Vector2( get_parent().position.x, get_parent().position.y )
	var x_diff = fmod(( self.position.x - grid_width / 2 - grid_origin[0] ) , grid_width )
	var y_diff = fmod(( self.position.y - grid_width / 2 - grid_origin[1] ) , grid_width )
	self.position.x -= x_diff
	self.position.y -= y_diff


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Coin_area_entered(area):
	$CoinSound.play()
	area.add_coin()


func _on_CoinSound_finished():
	self.queue_free()
