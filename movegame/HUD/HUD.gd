extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# these are placeholders:
var num_left_moves
var num_up_moves
var num_down_moves
var num_right_moves
var num_trinkets_collected
var num_trinkets_total
onready var left_label = $CenterContainer/MarginContainer/MarginContainer/CenterContainer/HBoxContainer/left_cont/left_label
onready var up_label = $CenterContainer/MarginContainer/MarginContainer/CenterContainer/HBoxContainer/up_cont/up_label
onready var down_label = $CenterContainer/MarginContainer/MarginContainer/CenterContainer/HBoxContainer/down_cont/down_label
onready var right_label = $CenterContainer/MarginContainer/MarginContainer/CenterContainer/HBoxContainer/right_cont/right_label
onready var trinket_label = $CenterContainer/MarginContainer/MarginContainer/CenterContainer/HBoxContainer/VBoxContainer/trinket_count

# Called when the node enters the scene tree for the first time.
func _ready():
	update_state(0,0,0,0,0,0)

func update_left(new_left_count):
	num_left_moves = new_left_count
	left_label.text = String(num_left_moves)

func update_up(new_up_count):
	num_up_moves = new_up_count
	up_label.text = String(num_up_moves)

func update_down(new_down_count):
	num_down_moves = new_down_count
	down_label.text = String(num_down_moves)

func update_right(new_right_count):
	num_right_moves = new_right_count
	right_label.text = String(num_right_moves)

func update_coin(num_collected, num_total):
	num_trinkets_collected = num_collected
	num_trinkets_total = num_total
	trinket_label.text = String(num_trinkets_collected) + "/" + String(num_trinkets_total)

func update_state(left, up, down, right, trinkets_collected, trinkets_tot):
	num_left_moves = left
	num_up_moves = up
	num_down_moves = down
	num_right_moves = right
	num_trinkets_collected = trinkets_collected
	num_trinkets_total = trinkets_tot
	update_HUD()

func update_HUD():
	left_label.text = String(num_left_moves)
	up_label.text = String(num_up_moves)
	down_label.text = String(num_down_moves)
	right_label.text = String(num_right_moves)
	trinket_label.text = String(num_trinkets_collected) + "/" + String(num_trinkets_total)
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
