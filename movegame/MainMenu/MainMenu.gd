extends MarginContainer


enum Buttons {NONE,LEVELS}
var selButton = Buttons.NONE
onready var level_select_button = $MarginContainer/HBoxContainer/VBoxContainer/LevelSelectButton

# Called when the node enters the scene tree for the first time.
func _ready():
	if get_viewport().size.x == 512:
		OS.set_window_resizable(false)
		get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_VIEWPORT, 
				SceneTree.STRETCH_ASPECT_EXPAND, 
				Vector2(get_viewport().size.x, get_viewport().size.y))
		var scaling = 1.5
		OS.set_window_size( scaling * Vector2(get_viewport().size.x, get_viewport().size.y))
		OS.set_window_position(Vector2(get_viewport().size.x / 1.5, get_viewport().size.y / 10))

func _input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed:
		if selButton == Buttons.LEVELS:
			get_tree().change_scene("res://MainMenu/LevelSelect.tscn")
			

func _on_LevelSelectButton_pressed():
	get_tree().change_scene("res://MainMenu/LevelSelect.tscn")
