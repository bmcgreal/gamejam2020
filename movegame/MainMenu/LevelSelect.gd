extends MarginContainer


func _ready():
	if get_viewport().size.x == 512:
		OS.set_window_resizable(false)
		get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_VIEWPORT, 
				SceneTree.STRETCH_ASPECT_EXPAND, 
				Vector2(get_viewport().size.x, get_viewport().size.y))
		var scaling = 1.5
		OS.set_window_size( scaling * Vector2(get_viewport().size.x, get_viewport().size.y))
		#OS.set_window_position(Vector2(get_viewport().size.x / 1.5, get_viewport().size.y / 10))

## add a signal for each button
## hardcode the scene change here
func _on_11Button_pressed():
	get_tree().change_scene("res://Levels/level_1_1.tscn")

func _on_12Button_pressed():
	get_tree().change_scene("res://Levels/level_1_2.tscn")

func _on_13Button_pressed():
	get_tree().change_scene("res://Levels/level_1_3.tscn")

func _on_14Button_pressed():
	get_tree().change_scene("res://Levels/level_1_4.tscn")

func _on_15Button_pressed():
	get_tree().change_scene("res://Levels/level_1_5.tscn")


func _on_21Button_pressed():
	get_tree().change_scene("res://Levels/level_2_1.tscn")

func _on_22Button_pressed():
	get_tree().change_scene("res://Levels/level_2_2.tscn")

func _on_23Button_pressed():
	get_tree().change_scene("res://Levels/level_2_3.tscn")

func _on_24Button_pressed():
	get_tree().change_scene("res://Levels/level_2_4.tscn")

func _on_25Button_pressed():
	get_tree().change_scene("res://Levels/level_2_5.tscn")

func _on_31Button_pressed():
	get_tree().change_scene("res://Levels/level_3_1.tscn")

func _on_32Button_pressed():
	get_tree().change_scene("res://Levels/level_3_2.tscn")

func _on_33Button_pressed():
	get_tree().change_scene("res://Levels/level_3_3.tscn")

func _on_34Button_pressed():
	get_tree().change_scene("res://Levels/level_3_4.tscn")

func _on_35Button_pressed():
	get_tree().change_scene("res://Levels/level_3_5.tscn")

func _on_BackButton_pressed():
	get_tree().change_scene("res://MainMenu/MainMenu.tscn")
