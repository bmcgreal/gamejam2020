extends PopupMenu


var next_level_path

func set_next_level_path(var level_path):
	next_level_path = level_path

func _on_NextLevelButton_pressed():
	get_tree().change_scene(next_level_path)

func _on_BackToMenuButton_pressed():
	get_tree().change_scene("res://MainMenu/LevelSelect.tscn")
